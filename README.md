# MASTODON - Buscar palabras en tus toots

Programa simple en Python para poder buscar palabras que aparecen en tus propios toots desde el archivo de copia de seguridad. Util si tu instancia ha capado esa opción. Ejecutarlo en la misma carpeta que esté 'outbox.json'

### Notas de interés

- Busca desde el archivo 'outbox.json' que se puede obtener en la url `https://tuinstancia/settings/export` (cambiar `tuinstancia` por la url de tu propia instancia)
- Es una busqueda muy simple. Si buscas `hola` te dará toots con la palabra `hola` pero también `holahola`, `rehola`... y tienen que aparecer TODAS las palabras que quieras buscar. Si una de ellas no aparece el toot es descartado
- Hay que ejecutarlo con Python3
- Si desde windows da error de codificación al leer el archivo de toots descomentar la línea 11 `f = open(arc,'r',encoding="utf8")` y comentar la línea 12 `f = open(arc,'r')`
