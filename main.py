# -*- coding: utf-8 -*-
import json
import re

def quitar_html(html):
  patron = re.compile('<.*?>')
  return re.sub(patron, '', html)

def leer_archivo(arc):
    # outbox.json
    #f = open(arc,'r',encoding="utf8") # usar en windows si te da error
    f = open(arc,'r')
    toots = f.read()
    f.close()
    return toots
  
def sacar_url_web(id_):
    id_ = id_.split('/')
    return id_[0]+'//'+id_[2]+'/web/statuses/'+id_[-1]
    
def buscar(busqueda,toots):
    estados = []
    
    # separamos en palabras la busqueda
    busqueda = busqueda.split()
    
    # recorremos todos los toots
    for toot in toots["orderedItems"]:
        encontrado = False
        try:
            estado = quitar_html(toot['object']['content'])
        except:
            continue
        # recorremos todas las palabras
        for palabra in busqueda:
            if palabra in estado:
                encontrado = True
            else:
                encontrado = False
                break
        if encontrado:
            estados.append({'url':sacar_url_web(toot['object']['id']),
                            'estado':estado})
    return estados
    
if __name__ == "__main__":
    print('Cargando archivo de toots...')
    toots = leer_archivo('outbox.json')
    toots = json.loads(toots)
    print('Total de elementos: '+str(toots["totalItems"]))
        
    print("¿Que quiere buscar?")
    busqueda = input()
    
    print('Buscando palabras...')
    estados = buscar(busqueda,toots)
    
    print('Encontrados '+str(len(estados))+' estados:')
    for est in estados:
        print('--------------------------------------------------------------')
        print('URL:')
        print('    '+est['url'])
        print('ESTADO:')
        print(est['estado'])
        print('')
